
/*********** Vorbereitungen *****************/
//export einträge in das profile eintragen

nano ~/.profile

//eintragen
export SLOTS=/sys/devices/bone_capemgr.9/slots
export PINS=/sys/kernel/debug/pinctrl/44e10800.pinmux/pins

source //variablen setzen

//kontrolle
cat $SLOTS
sudo cat $PINS

//permanentes Eintragen der variablen
visudo

Defaults    env_reset
Defaults    env_keep += "SLOTS"
Defaults    env_keep += "PINS"


/**************** Kompilieren ******************/
dtc -O dtb -o example-00A0.dtbo -b 0 -@ example.dts
sudo cp example-00A0.dtbo /lib/firmware

/**************** Einbinden ********************/
sudo su
echo example >$SLOTS
cat $SLOTS

//ausgabe der geladenen device trees

/*************** Entfernen ********************/
echo -6 > $SLOTS
