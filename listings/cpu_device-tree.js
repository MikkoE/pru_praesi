


/include/ "am33xx.dtsi"

/ {
  model = "TI AM335x BeagleBone";
  compatible = "timan335x-bone", "ti,am33xx";

}
cpus {
  cpu@0 {
    cpu0-supply = <&dcdc2_reg>;
  };
};
